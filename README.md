# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://KnezT@bitbucket.org/KnezT/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/KnezT/stroboskop/commits/e7ee1585657ce8762e35be6c20980999356546bc

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/KnezT/stroboskop/commits/1de97297bebe030c8756201efb8e72de4dc9419f

Naloga 6.3.2:
https://bitbucket.org/KnezT/stroboskop/commits/d1d1ded17a584b995171dc20e34aef830b40cc9d

Naloga 6.3.3:
https://bitbucket.org/KnezT/stroboskop/commits/966c62ff00a19fe8b093706d5f7046aa02cccf89

Naloga 6.3.4:
https://bitbucket.org/KnezT/stroboskop/commits/7043a2bf25943f55e6c47c84dd04efd0e5622596

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/KnezT/stroboskop/commits/0da2dcfa9efbea785b4c16ae3fc3f63f33311b59

Naloga 6.4.2:
https://bitbucket.org/KnezT/stroboskop/commits/401c59db49d606f8baefe784926916f257d2c9ee

Naloga 6.4.3:
https://bitbucket.org/KnezT/stroboskop/commits/01fa5a3612fdc8122d95ad4f7016336ad7a5886d

Naloga 6.4.4:
https://bitbucket.org/KnezT/stroboskop/commits/c9a446b0a0bf802943c04d629125b0314d5f9327